import modin.pandas as pd
import numpy as np

df = pd.read_csv('droped_unf.csv')
df = df.drop(labels=['MOVIES_MINUTES_VIEWED', 'MOVIES_STARTED'], axis=1)
missing_string_columns = ['FIRST_SHOW_MOVIE', 'FIRST_SHOW_LIBRARY']
missing_columns = ['DAYS_SINCE_LAST_COMPLETED_LONG_FORM',
 'DAYS_SINCE_LAST_5MIN',
 'PERC_REPEATED_MINUTES_ALL_LONG_FORMS',
 'MAX_LONG_FORM_STARTED',
 'PERC_MINUTES_TRAILERS_ORIG',
 'PERC_MINUTES_MOV_ORIG',
 'LONG_MINUTES_VIEWED',
 'PERC_IN_SEASON_MINUTES_TOTAL',
 'LONG_STARTED',
 'PERC_TOTAL_LIVE_MIN_VOD',
 'ASSETS_STARTED',
 'MINUTES_VIEWED',
 'PERC_6MIN_DAYS_ACTIVE',
 'MOVIES_ACTIVE_DAYS',
 'DAYS_SINCE_LAST',
 'PERC_6MIN_DAYS_ALL',
 'VELOCITY_3',
 'VELOCITY_2',
 'VELOCITY_1',
 'LONG_ACTIVE_DAYS',
 'FIRST_SHOW_MOVIE',
 'FIRST_SHOW_LIBRARY',
 'PERC_MINUTES_SHORT_ORIG',
 'ACTIVE_DAYS']

missing_float_columns = ['PERC_6MIN_DAYS_ACTIVE',
 'PERC_6MIN_DAYS_ALL',
 'ACTIVE_DAYS',
 'MAX_LONG_FORM_STARTED',
 'VELOCITY_1',
 'LONG_STARTED',
 'PERC_REPEATED_MINUTES_ALL_LONG_FORMS',
 'VELOCITY_2',
 'PERC_MINUTES_TRAILERS_ORIG',
 'LONG_ACTIVE_DAYS',
 'MOVIES_ACTIVE_DAYS',
 'ASSETS_STARTED',
 'DAYS_SINCE_LAST',
 'VELOCITY_3',
 'PERC_TOTAL_LIVE_MIN_VOD',
 'LONG_MINUTES_VIEWED',
 'MINUTES_VIEWED',
 'PERC_MINUTES_MOV_ORIG',
 'DAYS_SINCE_LAST_COMPLETED_LONG_FORM',
 'PERC_IN_SEASON_MINUTES_TOTAL',
 'DAYS_SINCE_LAST_5MIN',
 'PERC_MINUTES_SHORT_ORIG']

float_columns = df.select_dtypes(include=['float64']).columns.tolist()
global_min = min([df[col].min() for col in float_columns])
 
 
e99 = df.VELOCITY_1.max() / 10
 
start_values = df.loc[df.DAYS_SUBSCRIBED == 1, missing_float_columns]
end_values = df.loc[df.DAYS_BEFORE_SUBSCRIPTION_END == 1, missing_float_columns]

start_values = df.loc[df.DAYS_SUBSCRIBED == 1, missing_float_columns]
end_values = df.loc[df.DAYS_BEFORE_SUBSCRIPTION_END == 1, missing_float_columns]

interpolate_start_val = dict()
interpolate_end_val = dict()

for col in missing_float_columns:
    z = start_values[col]
    interpolate_start_val[col] = z.loc[z < e99 * 10].mean()
    z = start_values[col]
    interpolate_end_val[col] = z.loc[z < e99 * 10].mean()
    
def check_float_incorrect(column):
    a = column.isna().sum() == column.shape[0]
    if column.name in missing_float_columns:
        b = (column > e99).sum() > 0
    else:
        b = False
    c = column.isin([np.inf, -np.inf]).sum() > 0
    return a or b or c

def clean_data(x):
    round_size = x.shape[0]
    for col in missing_columns:
        if x[col].isnull().sum() > 0:
            if check_float_incorrect(x[col]):
                x[col] = global_min - 1
            elif x[col].nunique() == 1:
                x[col] = x[col].unique()[-1]
            else:
                if col in missing_string_columns:
                    x[col] = x[col].value_counts().index[0]
                else:
                    if np.isnan(x[col].iloc[0]):
                        inc = x[col].dropna().is_monotonic_increasing
                        dec = x[col].dropna().is_monotonic_decreasing
                        if inc:
                            val = x[col].min()
                        elif dec:
                            val = x[col].max()
                        else:
                            val = interpolate_start_val[col]
                        x[col].iloc[0] = val
                    if np.isnan(x[col].iloc[-1]):
                        if inc:
                            val = x[col].max()
                        elif dec:
                            val = x[col].min()
                        else:
                            val = interpolate_end_val[col]
                        x[col].iloc[-1] = val
                    x[col] = x[col].interpolate()
                    
    return x

print('Started cleaning')    
cleaned_data = df.groupby(['LABEL', 'PERIOD_RN']).apply(clean_data)
print('Ended cleaning')
cleaned_data.to_csv('cleaned_data.csv')
