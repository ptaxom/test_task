# Решение тестового задания

Решение было произведено в 3 этапа:
1. Очистка данных(<a href="https://gitlab.com/ptaxom/test_task/blob/master/DataCleaning.ipynb">DataCleaning.ipynb</a>)
2. Анализ данных(<a href="https://gitlab.com/ptaxom/test_task/blob/master/DataAnalisys.ipynb">DataAnalisys.ipynb</a>)
3. Построение модели(<a href="https://gitlab.com/ptaxom/test_task/blob/master/Predicting.ipynb">Predicting.ipynb</a>)
